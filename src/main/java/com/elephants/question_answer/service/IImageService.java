package com.elephants.question_answer.service;

import com.elephants.question_answer.models.Images;

public interface IImageService {

	Images save (Images image);
	
}
