package com.elephants.question_answer.service;

import java.util.List;

import com.elephants.question_answer.models.Comments;
import com.elephants.question_answer.models.Questions;

public interface ICommentService {
	
	Comments save(Comments comment);
	List<Comments> findAll();
	Comments findOneById(String id);
	Comments updateComment(Comments comment);
	Comments likeComment(Comments comment);
	String deleteComment(String id);
	List<Comments> findByIdQuestion(Questions question);

}
