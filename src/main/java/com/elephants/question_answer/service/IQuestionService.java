package com.elephants.question_answer.service;

import java.util.List;

import com.elephants.question_answer.models.Questions;

public interface IQuestionService {
	
	List<Questions> findAll();
	Questions Save(Questions question/* , MultipartFile image */);
	Questions findOneById(String id);
	String DeleteById(String id);
	Questions UpdateQuestion(Questions question);
	List<Questions> findByIdSpecialist(String idS);


}
