package com.elephants.question_answer.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.elephants.question_answer.Repositorys.QuestionRepository;
import com.elephants.question_answer.Repositorys.SpecialistRepository;
import com.elephants.question_answer.models.Questions;
import com.elephants.question_answer.models.Questions.Status;
import com.elephants.question_answer.models.Roles;
import com.elephants.question_answer.models.Users;
import com.elephants.question_answer.service.IQuestionService;
import com.elephants.question_answer.service.IUserService;

@Service
public class QuestionService implements IQuestionService {

	@Autowired
	QuestionRepository questionRepository;

	@Autowired
	SpecialistRepository specialistRepository;
	
	@Autowired
	private IUserService userService;
	
//	private StorageService storageService;

	@Override
	public List<Questions> findAll() {
		return questionRepository.findAll();
	}

	@Override
	public Questions Save(Questions question/* , MultipartFile image */) {
//		Users user = userService.findOneById(question.getIdUser());
//		Images img;
//		if(user != null)
//		{
//			if(!image.isEmpty())
//			{
//				img = storageService.store(image);
//				if(img != null)
//				{
//					question.setIdImage(img.getId());
//				}
//			}
//			String [] role = user.getRole();
//	    	int length = role.length;
//	    	for(int i = 0; i < length; ++i)
//	    	{
//	    		if(role[i].toUpperCase().equals(Roles.USER))
//	    		{
	    			question.setView("0");
	    			question.setStatus(String.valueOf(Status.UNAPPROVED));
	    			return questionRepository.save(question);
//	    		}
//	    	}
//		}
//    	return null;
	}

	@Override
	public Questions findOneById(String id) {
		Questions existQuestions = questionRepository.findOneById(id);
		if(existQuestions != null)
		{
			existQuestions.setView(String.valueOf(Integer.parseInt(existQuestions.getView()) + 1 ));// view đang tăng 2
			return questionRepository.save(existQuestions);
		}
		return null;
	}

	@Override
	public String DeleteById(String id) {
		questionRepository.deleteById(id);
		// xóa image :
		return "xóa thành công";
	}

	@Override
	public Questions UpdateQuestion(Questions question) {
		// check xem tt khong
		Questions oldQuestion = questionRepository.findOneById(question.getId());
		if (oldQuestion != null) {
			{
				oldQuestion.setIdSpecialist(question.getIdSpecialist());
				oldQuestion.setStatus(question.getStatus());
				return questionRepository.save(oldQuestion);
			}
		}
		return null;
	}

	@Override
	public List<Questions> findByIdSpecialist(String idS) {
		return questionRepository.findAllByIdSpecialist(idS);
	}

}
