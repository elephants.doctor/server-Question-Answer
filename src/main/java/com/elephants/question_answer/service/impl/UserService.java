package com.elephants.question_answer.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.elephants.question_answer.Repositorys.UserRepository;
import com.elephants.question_answer.models.CustomUserDetails;
import com.elephants.question_answer.models.Images;
import com.elephants.question_answer.models.Roles;
import com.elephants.question_answer.models.Users;
import com.elephants.question_answer.security.jwt.JwtTokenProvider;
import com.elephants.question_answer.service.IUserService;
import com.elephants.question_answer.service.uploads.StorageService;

@Service
public class UserService implements IUserService, UserDetailsService {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	public JavaMailSender emailSender;
	
	@Autowired StorageService storageService;

	@Autowired
	PasswordEncoder passwordEncoder;
	@Override
	public Users save(Users user) {
		Users isEmailUser = userRepository.findOneByUserEmail(user.getUserEmail());
		Users isUserName = userRepository.findByUserName(user.getUserName());

		if(isUserName == null && isEmailUser == null)
		{
			user.setUserPass(passwordEncoder.encode(user.getUserPass()));
			String[] r = {Roles.USER};
	        user.setRole(r);
	        user.setIdImage(null);
			return userRepository.save(user);
		}
		else
		{
			return null;
		}
	}

	@Override
	public Users findOneByUserEmailAndUserPass(String email, String pass) {
		return userRepository.findOneByUserEmailAndUserPass(email, pass);
	}

	@Override
	public List<Users> findAll() {
		return userRepository.findAll();
	}

	@Override
	public Users findOneById(String id) {
		return userRepository.findOneById(id);
	}

	@Override
	public Users findOneByUserName(String name) {return userRepository.findOneByUserName(name);}

	@Override
	public Users findOneByUserEmail(String userEmail) {return userRepository.findOneByUserEmail(userEmail);}

	@Override
	public String deleteById(String id) {
		userRepository.deleteById(id);
		return "Xóa thành công";
	}

	@Override
	public Users UpdateUser(Users user) {
		// check xem tt khong
		Users oldUser = userRepository.findOneById(user.getId());
		if (oldUser != null) {
			oldUser.setUserName(user.getUserName());
			oldUser.setFullName(user.getFullName());
			oldUser.setUserPass(user.getUserPass());
			oldUser.setCellPhone(user.getCellPhone());
			oldUser.setDateOfBirth(user.getDateOfBirth());
			oldUser.setSex(user.getSex());
			oldUser.setRole(user.getRole());
			return userRepository.save(oldUser);
		}
		return null;
	}

	@Autowired
	private JwtTokenProvider tokenProvider;

	@Override
	public String forgotPassword(String emails) {
		Users user = userRepository.findOneByUserEmail(emails);
		if(user != null)// ton tai user
		{
			String email = user.getUserEmail();
			String password = user.getUserPass();
			String contentEmail = "your old pass: " + password;
			// tao Simple MailMessage :
			SimpleMailMessage message = new SimpleMailMessage();
			message.setTo(email);
			message.setSubject("Forgot password");
			message.setText(contentEmail);
			
			this.emailSender.send(message);
			return "Success";
		}
		return null;
	}


	@Override
	public UserDetails loadUserByUsername(String nameOrEmail) throws UsernameNotFoundException {
		// Kiểm tra xem user có tồn tại trong database không?
		Users isNameUser = userRepository.findOneByUserName(nameOrEmail);
		if(isNameUser != null)
			return new CustomUserDetails(isNameUser);

		Users isEmailUser = userRepository.findOneByUserEmail(nameOrEmail);
		 if (isEmailUser != null)
			return new CustomUserDetails(isEmailUser);


		 throw new UsernameNotFoundException(nameOrEmail);
	}

	@Override
	public Images UpdateImageUser(Users user, MultipartFile image) {
		Users oldUser = userRepository.findOneById(user.getId());
		if (oldUser != null) {
			Images img = storageService.store(image);
			if(img != null)
			{
				oldUser.setIdImage(img.getId());
			}
			userRepository.save(oldUser);
			return img;
		}
		return null;
	}
}
