package com.elephants.question_answer.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.elephants.question_answer.Repositorys.SpecialistRepository;
import com.elephants.question_answer.models.Specialist;
import com.elephants.question_answer.service.ISpecialistService;

@Service
public class SpecialistService implements ISpecialistService{
	
	@Autowired
	private SpecialistRepository specialistRepository;

	@Override
	public Specialist Save(Specialist specialist) {
		Specialist sp = specialistRepository.findOneByName(specialist.getName());
		if(sp == null)
		{
			return specialistRepository.save(specialist);
		}
		else
		{
			return null;
		}
	}

	@Override
	public List<Specialist> findAll() {
		return specialistRepository.findAll();
	}

	@Override
	public Specialist findOneById(String id) {
		return specialistRepository.findOneById(id);
	}

	@Override
	public Boolean removeById(String id) {
		try {
			specialistRepository.deleteById(id);
		return true;
		} catch (Exception e) {
			return false;
		}
		
	}

	@Override
	public Specialist updateSpecialist(Specialist specialist) {
		specialistRepository.save(specialist);
		return specialist;
	}
}
