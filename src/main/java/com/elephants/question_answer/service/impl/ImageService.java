package com.elephants.question_answer.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.elephants.question_answer.Repositorys.ImageRepository;
import com.elephants.question_answer.models.Images;
import com.elephants.question_answer.service.IImageService;

@Service
public class ImageService implements IImageService{

	@Autowired
	private ImageRepository imageRepository;
	
	@Override
	public Images save(Images image) {
		try
		{
			Images img = imageRepository.save(image);
			return img;
		}
		catch (Exception e) {
			return null;
		}
	}

}
