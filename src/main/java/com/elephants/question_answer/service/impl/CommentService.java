package com.elephants.question_answer.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.elephants.question_answer.Repositorys.CommentRepository;
import com.elephants.question_answer.models.Comments;
import com.elephants.question_answer.models.Questions;
import com.elephants.question_answer.models.Roles;
import com.elephants.question_answer.models.Users;
import com.elephants.question_answer.service.ICommentService;
import com.elephants.question_answer.service.IQuestionService;
import com.elephants.question_answer.service.IUserService;

@Service
public class CommentService implements ICommentService{
	
	@Autowired
	private CommentRepository commentRepository;
	
	@Autowired
	private IQuestionService questionService;
	
	@Autowired
	private IUserService userService;

	@Override
	public Comments save(Comments comment) {
		Questions question = questionService.findOneById(comment.getIdQuestion());
		if(question != null)
		{
			Users userResponse = userService.findOneById(comment.getIdUserResponse());
			Users userRequest = userService.findOneById(comment.getIdUser());

			if(userRequest != null && userResponse != null)
			{
				String [] role = userRequest.getRole();
		    	int length = role.length;
		    	for(int i = 0; i < length; ++i)
		    	{

		    		if(role[i].toUpperCase().equals(Roles.DOCTOR))
		    		{
		    			comment.setLikes(0);
		    			return commentRepository.save(comment);
		    		}
		    	}
			}
		}
    	return null;	
	}

	@Override
	public List<Comments> findAll() {
		return commentRepository.findAll();
	}

	@Override
	public Comments findOneById(String id) {
		Comments existComment = commentRepository.findOneById(id);
		if(existComment == null)
		{
			return null;
		}
		return existComment;
	}

	@Override
	public Comments updateComment(Comments comment) {
		Comments existComment = commentRepository.findOneById(comment.getId());
		if(existComment != null)
		{
			existComment.setIdQuestion(comment.getIdQuestion());
			existComment.setIdUser(comment.getIdUser());
			existComment.setIdUserResponse(comment.getIdUserResponse());
			existComment.setContent(comment.getContent());
			return commentRepository.save(existComment);
		}
		return null;
	}

	@Override
	public Comments likeComment(Comments comment) {
		Comments existComment = commentRepository.findOneById(comment.getId());
		if(existComment != null)
		{
			existComment.setLikes(existComment.getLikes()  +1 );
			return commentRepository.save(existComment);
		}
		return null;
	}

	@Override
	public String deleteComment(String id) {
		commentRepository.deleteById(id);
		return "đã xóa";
	}

	@Override
	public List<Comments> findByIdQuestion(Questions question) {
		Questions existQuestion = questionService.findOneById(question.getId());
		List<Comments> existComment = null;
		if(existQuestion != null)
		{
			existComment = commentRepository.findByIdQuestion(existQuestion.getId());
		}
		return existComment;
	}
	
	

}
