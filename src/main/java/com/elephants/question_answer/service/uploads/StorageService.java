package com.elephants.question_answer.service.uploads;

import java.nio.file.Path;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import com.elephants.question_answer.models.Images;

public interface StorageService {

	void init();

	Images store(MultipartFile file);

	Path load(String filename);
	
//	Stream<Path> loadAll();
//
	Resource loadAsResource(String filename);
//
//	void deleteAll();

}
