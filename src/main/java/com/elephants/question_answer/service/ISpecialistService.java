package com.elephants.question_answer.service;

import java.util.List;

import com.elephants.question_answer.models.Specialist;

public interface ISpecialistService {
	
	Specialist Save (Specialist specialist);
	List<Specialist> findAll();
	Specialist findOneById (String id);
	Boolean removeById (String id);
	Specialist updateSpecialist(Specialist specialist);
}
