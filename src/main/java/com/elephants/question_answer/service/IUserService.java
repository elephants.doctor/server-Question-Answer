package com.elephants.question_answer.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.elephants.question_answer.models.Images;
import com.elephants.question_answer.models.Users;

public interface IUserService {

	Users save(Users user);
	Users findOneByUserEmailAndUserPass(String email, String pass);
	List<Users> findAll();
	Users findOneById(String id);
	String deleteById(String id);
	Users UpdateUser(Users user);
	Images UpdateImageUser(Users user, MultipartFile image);
	String forgotPassword(String emails);
	Users findOneByUserName(String name);
	Users findOneByUserEmail(String name);

}
