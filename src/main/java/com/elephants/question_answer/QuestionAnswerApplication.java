package com.elephants.question_answer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.elephants.question_answer.Repositorys.UserRepository;
import com.elephants.question_answer.service.uploads.StorageProperties;

@SpringBootApplication
@EnableConfigurationProperties(StorageProperties.class)// 
public class QuestionAnswerApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(QuestionAnswerApplication.class, args);
    }
    @Autowired
    UserRepository userRepository;

    @Override
    public void run(String... args) throws Exception {
    }
}
