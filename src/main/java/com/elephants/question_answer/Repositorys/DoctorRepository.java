package com.elephants.question_answer.Repositorys;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.elephants.question_answer.models.Doctors;

public interface DoctorRepository extends MongoRepository<Doctors, String>{

}
