package com.elephants.question_answer.Repositorys;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.elephants.question_answer.models.Users;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<Users, String> {
	
	Users findOneByUserEmail(String email);
	Users findOneByUserEmailAndUserPass(String userEmail, String pass);
	Users findByUserName(String userName);
	Users findOneById(String id);
	Users findOneByUserName(String userName);
}
