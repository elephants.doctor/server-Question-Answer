package com.elephants.question_answer.Repositorys;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.elephants.question_answer.models.Specialist;

public interface SpecialistRepository extends MongoRepository<Specialist, String>{
	
	Specialist findOneByName(String name);
	Specialist findOneById(String id);

}
