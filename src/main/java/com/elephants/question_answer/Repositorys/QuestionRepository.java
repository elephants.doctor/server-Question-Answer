package com.elephants.question_answer.Repositorys;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.elephants.question_answer.models.Questions;

import java.util.List;

public interface QuestionRepository extends MongoRepository<Questions, String>{
	
	Questions findOneById(String id);
	List<Questions> findAllByIdSpecialist(String idS);
}
