package com.elephants.question_answer.Repositorys;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.elephants.question_answer.models.Comments;

public interface CommentRepository extends MongoRepository<Comments, String>{
	
	Comments findOneById(String id);
	List<Comments> findByIdQuestion(String id);
	
}
