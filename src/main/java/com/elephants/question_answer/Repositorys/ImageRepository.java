package com.elephants.question_answer.Repositorys;

import com.elephants.question_answer.models.Questions;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.elephants.question_answer.models.Images;

public interface ImageRepository extends MongoRepository<Images, String>{
    Images findOneById(String id);

}
