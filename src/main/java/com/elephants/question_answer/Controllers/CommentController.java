package com.elephants.question_answer.Controllers;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.elephants.question_answer.models.Comments;
import com.elephants.question_answer.models.Questions;
import com.elephants.question_answer.models.ResponseHttp;
import com.elephants.question_answer.service.ICommentService;
import com.elephants.question_answer.service.IUserService;

@RestController
@RequestMapping("/comment")
public class CommentController {

	@Autowired
	private ICommentService commentService;

	@Autowired
	private IUserService userService;

	@Autowired
	public JavaMailSender emailSender;

	@PostMapping(value = "/addcomment", consumes = "application/json", produces = "application/json")
	public Object addComment(@RequestBody Comments comment) {

		Comments toComment = commentService.save(comment);
		if (toComment == null) {
			ResponseHttp resp = new ResponseHttp("add comment is failed ", HttpServletResponse.SC_CREATED);
			return resp.getStringResponse();
		} else {
			try {
				SimpleMailMessage message = new SimpleMailMessage();
				message.setTo(userService.findOneById(toComment.getIdUserResponse()).getUserEmail());
				message.setSubject("Trả lời câu hỏi");

				message.setText("Bác sĩ đã trả lời câu hỏi của bạn : "
						+ toComment.getContent().substring(0, comment.getContent().indexOf(" ", 3))
						+ "...   Nhấn để xem chi tiết  : "
						+ "https://question-answer.invocker.repl.co/detail-question/detail-question.php?id="
						+ comment.getIdQuestion());
				this.emailSender.send(message);
			} catch (MailException e) {
				e.printStackTrace();
			}

		}
		return toComment;

	}

	@GetMapping(value = "/listcomment", consumes = "application/json", produces = "application/json")
	public List<Comments> listComment() {
		return commentService.findAll();
	}

	@PostMapping(value = "/findbyquestion", consumes = "application/json", produces = "application/json")
	public Object findCommentByQuestion(@RequestBody Questions question) {
		List<Comments> comments = commentService.findByIdQuestion(question);
		if (comments == null) {
			ResponseHttp resp = new ResponseHttp("comment does not exist", HttpServletResponse.SC_UNAUTHORIZED);
			return resp.getStringResponse();
		}
		return comments;
	}

	@PostMapping(value = "/comment", consumes = "application/json", produces = "application/json")
	public Object findOneComment(@RequestBody Map<String, String> ids) {
		Comments comment = commentService.findOneById(ids.get("id"));
		if (comment == null) {
			ResponseHttp resp = new ResponseHttp("comment does not exist", HttpServletResponse.SC_UNAUTHORIZED);
			return resp.getStringResponse();
		}
		return comment;
	}

	@PutMapping(value = "/updatecomment", consumes = "application/json", produces = "application/json")
	public Object updateComment(@RequestBody Comments comment) {
		Comments cmt = commentService.updateComment(comment);
		if (cmt == null) {
			ResponseHttp resp = new ResponseHttp("update failed ", HttpServletResponse.SC_UNAUTHORIZED);
			return resp.getStringResponse();
		}
		return cmt;
	}

	@PutMapping(value = "/like", consumes = "application/json", produces = "application/json")
	public Object likeComment(HttpServletRequest request, @RequestBody Comments comment) {
		if (request.getAttribute("isUser") == "false") {
			return null;
		}
		Comments cmt = commentService.likeComment(comment);
		if (cmt == null) {
			ResponseHttp resp = new ResponseHttp("update failed ", HttpServletResponse.SC_CREATED);
			return resp.getStringResponse();
		}
		return cmt;
	}

	@DeleteMapping(value = "/deletecomment", consumes = "application/json", produces = "application/json")
	public String deleteComment(@RequestBody Map<String, String> ids) {
		return commentService.deleteComment(ids.get("id"));
	}

}
