package com.elephants.question_answer.Controllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.elephants.question_answer.models.ResponseHttp;
import com.elephants.question_answer.models.Specialist;
import com.elephants.question_answer.service.ISpecialistService;

@RestController
@RequestMapping("/specialist")
public class SpecialistController {

	@Autowired
	private ISpecialistService specialistService;

	@GetMapping(value = "/list", consumes = "application/json", produces = "application/json")
	public List<Specialist> selectAllSpecialist() {
		return specialistService.findAll();
	}

	@PostMapping(value = "/addspecialist", consumes = "application/json", produces = "application/json")
	public Specialist addSpecialist(@RequestBody Specialist specialist) {
		return specialistService.Save(specialist);
	}

	@PostMapping(consumes = "application/json", produces = "application/json")
	public Specialist selectOneSpecialist(@RequestBody Map<String, String> id) {
		return specialistService.findOneById(id.get("id"));
	}

	@DeleteMapping(value = "/remove", consumes = "application/json", produces = "application/json")
	public boolean removeSpecialist(@RequestBody Map<String, String> data) {
		return specialistService.removeById(data.get("id"));
	}

	@PutMapping(consumes = "application/json", produces = "application/json")
	public Object updateSpecialist(@RequestBody Specialist specialist) {

		if (specialistService.findOneById(specialist.getId()) == null) {
			ResponseHttp resHttp = new ResponseHttp("Specialist not exist",404);
			return resHttp.getStringResponse();
			
	   }
		return specialistService.Save(specialist);
	}
}
