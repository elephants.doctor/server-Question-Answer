package com.elephants.question_answer.Controllers;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.elephants.question_answer.models.Questions;
import com.elephants.question_answer.models.ResponseHttp;
import com.elephants.question_answer.service.IQuestionService;

@RestController
@RequestMapping("/question")
public class QuestionController {
	
	@Autowired
    private IQuestionService questionService;
	
	@Autowired
    AuthenticationManager authenManager;

    @GetMapping(value = "/list", consumes = "application/json", produces = "application/json")
    public List<Questions> selectAllQuestion(HttpServletResponse resp){
    	if(questionService.findAll() == null)
    	{
    		resp.setStatus(HttpServletResponse.SC_FOUND);
    		return null;
    	}
    	return questionService.findAll();
    }
    
    @PostMapping(value = "/add-question", consumes = "application/json", produces = "application/json")
	public Object addQuestion(@RequestBody Questions question/* , @RequestParam("image") MultipartFile image */)
    {
    	Questions isQuestion = questionService.Save(question);
    	if(isQuestion == null)
    	{
    		ResponseHttp resp = new ResponseHttp("add question is failer", HttpServletResponse.SC_UNAUTHORIZED);
    		return resp.getStringResponse();
    	}
    	return isQuestion;
    }
    
    @PostMapping(value = "/question", consumes = "application/json", produces = "application/json")
    public Questions findOneQuestion(@RequestBody Map<String, String> id, HttpServletResponse resp)
    {
    	if(questionService.findOneById(id.get("id")) == null)
    	{
    		resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    	}
    	return questionService.findOneById(id.get("id"));
    }

	@PostMapping(value = "/id_specialist", consumes = "application/json", produces = "application/json")
	public List<Questions> findByIdSpecialist(@RequestBody Map<String, String> data)
	{
		return questionService.findByIdSpecialist(data.get("idSpecialist"));
	}

    @DeleteMapping(value = "/deleteQuestion", consumes = "application/json", produces = "application/json")
    public String deleteQuestion(@RequestBody Map<String, String> id, HttpServletResponse resp)
    {
    	return questionService.DeleteById(id.get("id"));
    }
    
    @PutMapping(value = "/updateQuestion", consumes = "application/json", produces = "application/json")
    public Questions updateQuestion(@RequestBody Questions question)
    {
    	return questionService.UpdateQuestion(question);
    }

}
