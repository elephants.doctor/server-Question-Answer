package com.elephants.question_answer.Controllers;

import java.util.Map;
import java.util.Objects;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.elephants.question_answer.models.CustomUserDetails;
import com.elephants.question_answer.models.Images;
import com.elephants.question_answer.models.ResponseHttp;
import com.elephants.question_answer.models.Users;
import com.elephants.question_answer.models.payload.LoginRequest;
import com.elephants.question_answer.models.payload.loginResponse;
import com.elephants.question_answer.security.jwt.JwtTokenProvider;
import com.elephants.question_answer.service.IUserService;

@RestController
@RequestMapping("/user")
public class UserControllers {

    @Autowired
    private IUserService userService;

    @GetMapping(value = "/{userName}", consumes = "application/json", produces = "application/json")
    public Users getUserByUserName(@PathVariable String userName){
        return userService.findOneByUserName(userName);
    }
    @GetMapping(value = "/email/{userEmail}", consumes = "application/json", produces = "application/json")
    public Users getUserByEmail(@PathVariable String userEmail){
        return userService.findOneByUserEmail(userEmail);
    }
	@PostMapping(value = "/register", consumes = "application/json", produces = "application/json")
    public Object addUser(@RequestBody Users user){
        Users u = userService.save(user);
        if(u == null)
        {
        	ResponseHttp resp = new ResponseHttp("email or user name is exist", HttpServletResponse.SC_CREATED);
        	return resp.getStringResponse();
        }
        return u;
    }

    @Autowired
    AuthenticationManager authenManager;

    @Autowired
    private JwtTokenProvider token;

    @PostMapping("/login")
    public loginResponse Login(@RequestBody LoginRequest req, HttpServletResponse responseHttp) {

        // Xác thực từ username và password.
        Authentication authentication;
        try {
            authentication = authenticate(req.getUsername(), req.getPassword());
            // Nếu không xảy ra exception tức là thông tin hợp lệ
            // Set thông tin authentication vào Security Context
            SecurityContextHolder.getContext().setAuthentication(authentication);

            // Trả về jwt cho người dùng.
            String jwt = token.generateToken((CustomUserDetails) authentication.getPrincipal());
            return new loginResponse(jwt);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            responseHttp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return null;
        }

    }
    private Authentication authenticate(String username, String password) throws Exception {
        Objects.requireNonNull(username);
        Objects.requireNonNull(password);

        try {
            return authenManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));

        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }

    }

    @DeleteMapping(value = "/deleteUser", consumes = "application/json", produces = "application/json")
    public String deleteUser(@RequestBody Map<String, String> id, HttpServletResponse resp)
    {
    	return userService.deleteById(id.get("id"));
    }

    @PutMapping(value = "/updateUser", consumes = "application/json", produces = "application/json")
	public Object updateUser(@RequestBody Users users)
    {
    	Users user = userService.UpdateUser(users);
    	if(user == null)
    	{
    		ResponseHttp resp = new ResponseHttp("update is failer", HttpServletResponse.SC_UNAUTHORIZED);
    		return resp.getStringResponse();
    	}
    	return user;
    }

	@PutMapping(value = "/updateImageUser"/* , consumes = "application/json", produces = "application/json" */)
	public Object updateImageUser(@RequestBody Users users, @RequestParam("image") MultipartFile image )
    {
    	Images img = userService.UpdateImageUser(users, image);
    	if(img == null)
    	{
    		ResponseHttp resp = new ResponseHttp("update is failer", HttpServletResponse.SC_UNAUTHORIZED);
    		return resp.getStringResponse();
    	}
    	return img;
    }

    @PostMapping(value = "/forgotpassword", consumes = "application/json", produces = "application/json")
    public String forgotPassword(@RequestBody Map<String, String> emails)
    {
    	String message = userService.forgotPassword(emails.get("userEmail"));
    	if(message == null)
    	{
    		ResponseHttp resp = new ResponseHttp("invalid email !!!", HttpServletResponse.SC_UNAUTHORIZED);
    		return resp.getStringResponse();
    	}
		return "Mail Sent !";
    }

    @PostMapping(value = "/id", consumes = "application/json", produces = "application/json")
    public Users getUserById(@RequestBody Map<String, String> data){
        return userService. findOneById(data.get("id"));
    }

}
