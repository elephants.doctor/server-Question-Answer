package com.elephants.question_answer.Controllers.Admin;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.elephants.question_answer.models.Users;
import com.elephants.question_answer.service.IUserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/admin")
public class AdminController {
	
	@Autowired
	IUserService userService;

	@GetMapping(value = "/quantri/listuser", consumes = "application/json", produces = "application/json")
	public List<Users> listUser(HttpServletRequest request, HttpServletResponse response)
	{
		if(request.getAttribute("isUser") == "false"){
			return null;
		}
		return userService.findAll();
	}
	
	@GetMapping(value = "/quantri/user", consumes = "application/json", produces = "application/json")
	public Object findOneUser(@RequestBody Map<String, String> id)
	{
		if(userService.findOneById(id.get("id")) == null)
    	{
//			ResponseHttp resHttp = new ResponseHttp("User not exist",404);
//			return resHttp.getStringResponse();
			return null;
    	}
    	return userService.findOneById(id.get("id"));
	}
	
	@PostMapping(value = "/quantri/adduser", consumes = "application/json", produces = "application/json")
	public Object addUser(@RequestBody Users users)
	{
		Users user = userService.save(users);
		if(user == null)
		{
//			ResponseHttp resp = new ResponseHttp("New addition failed !", HttpServletResponse.SC_UNAUTHORIZED);
//			return resp.getStringResponse();
			return null;
		}
		return user;
	}
	
	@DeleteMapping(value = "/quantri/deleteuser", consumes = "application/json", produces = "application/json")
	public String deleteUser(@RequestBody List<String> ids)
	{
		int index = 0;
		for (int i = 0; i < ids.size(); i++)
		{
			String u = userService.deleteById(ids.get(i));
			if(u != null)
			{
				index++;
			}
		}
		return "đã xóa : " + index + " user !";
	}
	
	@PutMapping(value = "/quantri/updateuser", consumes = "application/json", produces = "application/json")
	public Object updateUser(@RequestBody Users users)
	{
		Users user = userService.UpdateUser(users);
		if(user == null)
		{
//			ResponseHttp resp = new ResponseHttp("update failed !", HttpServletResponse.SC_UNAUTHORIZED);
//			return resp.getStringResponse();
			return null;
		}
		return user;
	}
	
}
