package com.elephants.question_answer.Controllers;

import javax.servlet.http.HttpServletResponse;

import com.elephants.question_answer.Repositorys.ImageRepository;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.elephants.question_answer.models.Images;
import com.elephants.question_answer.models.ResponseHttp;
import com.elephants.question_answer.service.uploads.StorageProperties;
import com.elephants.question_answer.service.uploads.StorageService;
import com.google.common.net.HttpHeaders;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping(value = "/image")
public class ImageController {
	
	private final StorageService storageService;
	@Autowired
	ImageRepository imageRepository;
	@Autowired StorageProperties storageUrl;
	
	@Autowired
	public ImageController(StorageService storageService) {
		this.storageService = storageService;
	}
	
	@PostMapping(value = "/addimage")
	public Object addImage(@RequestParam("image") MultipartFile image, RedirectAttributes redirectAttributes)
	{
		
		Images img = storageService.store(image);
//		redirectAttributes.addFlashAttribute("message",
//				"You successfully uploaded " + image.getOriginalFilename() + "!");
//		return "redirect:/file";
		if(img == null)
		{
			ResponseHttp resp = new ResponseHttp("New addition failed ", HttpServletResponse.SC_UNAUTHORIZED);
			return resp.getStringResponse();
		}
		return img;
	}
	
	
	@GetMapping("/{filename:.+}")
	@ResponseBody
	public ResponseEntity<org.springframework.core.io.Resource> serveFile(@PathVariable String filename) {

		org.springframework.core.io.Resource file = storageService.loadAsResource(filename);
		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
				"attachment; filename=\"" + file.getFilename() + "\"").body(file);
	}
	
	@PostMapping("/id/{id}")
	@ResponseBody
	public ResponseEntity<org.springframework.core.io.Resource> serverFile(@PathVariable String id) {
		Images nameImage = imageRepository.findOneById(id);
		String filename = nameImage.getName();

		org.springframework.core.io.Resource file = storageService.loadAsResource(filename);
		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
				"attachment; filename=\"" + file.getFilename() + "\"").body(file);
	}
	@GetMapping("/id/{id}")
	@ResponseBody
	public String serverFileString(@PathVariable String id, HttpServletResponse response) throws IOException {
		Images nameImage = imageRepository.findOneById(id);
		String filename = nameImage.getName();

		Path root = Paths.get("src/main/resources/");
		String url = String.valueOf(root.resolve(filename));
		File file1 = new File(url);

		FileInputStream fileInputStreamReader = new FileInputStream(file1);
		byte[] bytes = new byte[(int)file1.length()];
		fileInputStreamReader.read(bytes);

		return new String(Base64.encodeBase64(bytes), "UTF-8");
	}
//	@GetMapping("/all")
//	public String listUploadedFiles() throws IOException {
// 
//		 List<String> images =  storageService.loadAll().map(
//				path -> MvcUriComponentsBuilder.fromMethodName(ImageController.class,
//				"serveFile", path.getFileName().toString()).build().toUri().toString())
//				.collect(Collectors.toList());
//		 images.forEach((image) -> {
//			System.out.println(image); 
//		 });
//		return "uploadForm";
//	}
}
