package com.elephants.question_answer.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@Document(collection = "images")
public class Images extends AbstractAuditingEntity{
	private static final long serialVersionUID = 1L;
	public Images()
	{

	}
	@Id
	private String id;
	
	@Indexed
	private String name;
	private String url;

	public Images(String name, String url)
	{
		this.name = name;
		this.url = url;
	}

}
