package com.elephants.question_answer.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public final class Roles{

	public static final String ADMIN = "ADMIN";
	public static final String USER = "USER";
	public static final String DOCTOR = "DOCTOR";
	public static final String CENOR = "CENOR";
	
}
