package com.elephants.question_answer.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@Document(collection = "doctors")
public class Doctors extends AbstractAuditingEntity{
	private static final long serialVersionUID = 1L;

	@Id
	private String id;
	
	@Indexed
	private String idSpecialist;
	private String exp;
	private String workUnit;
	private Boolean isOnline;

}
