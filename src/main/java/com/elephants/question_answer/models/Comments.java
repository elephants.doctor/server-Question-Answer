package com.elephants.question_answer.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@Document(collection = "comments")
public class Comments extends AbstractAuditingEntity{
	private static final long serialVersionUID = 1L;

	@Id
	private String id;
	
	@Indexed
	private String idQuestion;
	private String idUser;
	private String idUserResponse;
	private String content;
	private int likes;
}