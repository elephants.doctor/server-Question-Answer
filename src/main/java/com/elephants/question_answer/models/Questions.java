package com.elephants.question_answer.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@Document(collection = "questions")
public class Questions extends AbstractAuditingEntity{
	private static final long serialVersionUID = 1L;
	
	public enum Status {APPROVED, UNAPPROVED, DONE};
	
	@Id
	private String id;
	
	@Indexed
	private String idSpecialist;
	private String content;
	private String idUser;
	private String view;
	private String status = String.valueOf(Status.UNAPPROVED);
	private String idImage;
	
}
