package com.elephants.question_answer.models.payload;

import lombok.Data;

@Data
public class LoginRequest {
    
    private String username;
    private String password;

}