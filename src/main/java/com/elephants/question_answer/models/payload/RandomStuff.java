package com.elephants.question_answer.models.payload;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RandomStuff {
    private String message;
}