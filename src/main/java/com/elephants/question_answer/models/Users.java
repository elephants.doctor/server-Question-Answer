package com.elephants.question_answer.models;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@Document(collection = "user")
public class Users extends AbstractAuditingEntity {
	private static final long serialVersionUID = 1L;
	
	@Id
	private String id;

	@Indexed
	private String userName;
	private String fullName;
	private String userPass;
	private String userEmail;
	private String cellPhone;
	private Date dateOfBirth;// chú ý : exception xảy ra khi không convert được java.util.date -> java.sql.date
	private String sex;
	private String[] role;
	private String idImage;
}
