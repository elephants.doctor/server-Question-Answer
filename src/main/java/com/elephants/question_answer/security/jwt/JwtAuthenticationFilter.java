package com.elephants.question_answer.security.jwt;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.FilterChain;
 import javax.servlet.ServletException;
 import javax.servlet.http.HttpServletRequest;
 import javax.servlet.http.HttpServletResponse;

import com.elephants.question_answer.service.impl.UserService;


 import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
 import org.springframework.security.core.context.SecurityContextHolder;
 import org.springframework.security.core.userdetails.UserDetails;
 import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
 import org.springframework.util.StringUtils;
 import org.springframework.web.filter.OncePerRequestFilter;

 import lombok.extern.slf4j.Slf4j;

 @Slf4j
 public class JwtAuthenticationFilter extends OncePerRequestFilter{
     @Autowired
     private JwtTokenProvider tokenProvider;

     @Autowired
     private UserService customUserDetailsService;
     @Override
     protected void doFilterInternal(HttpServletRequest request,
                                     HttpServletResponse response, FilterChain filterChain)
             throws ServletException, IOException {
             // Lấy jwt từ request
             String jwt = getJwtFromRequest(request);
             if (StringUtils.hasText(jwt) && tokenProvider.validateToken(jwt)) {
                 // Lấy  user từ chuỗi jwt
                 String userName = tokenProvider.getUserIdFromJWT(jwt);
                 // Lấy thông tin người dùng từ
                 UserDetails userDetails = customUserDetailsService.loadUserByUsername(userName);
                 if (userDetails != null) {
                     // Nếu người dùng hợp lệ, set thông tin cho Seturity Context
                     UsernamePasswordAuthenticationToken
                             authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                     authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                     SecurityContextHolder.getContext().setAuthentication(authentication);
                 }
             }
             else{
                 if(!isRouterPublic(request)){
                     request.setAttribute("isUser", "false");
                 }
             }

         filterChain.doFilter(request, response);
     }

     private boolean isRouterPublic(HttpServletRequest request) {
         String[] publicRouter = new String[]{"/user/login",
                 "/user/register",
                 "/swagger-ui.html",
                 "/user/forgotpassword,",
                 "/question/list",
                 "/comment/listcomment",
                 "/specialist/list",
                 "/user"};

         String uri = request.getRequestURI();

         return Arrays.stream(publicRouter).anyMatch(uri::equals) || checkMathBody(uri);
     }

     private boolean checkMathBody(String uri) {
         Pattern pattern = Pattern.compile("/user", Pattern.CASE_INSENSITIVE);
         Matcher matcher = pattern.matcher(uri);
         boolean matchFound = matcher.find();
         return matchFound;
     }

     private String getJwtFromRequest(HttpServletRequest request) {
         String bearerToken = request.getHeader("token-id");

         // Kiểm tra xem header Authorization có chứa thông tin jwt không
         if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
             return bearerToken.substring(7);
         }
         return null;
     }
 }