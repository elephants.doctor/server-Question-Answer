package com.elephants.question_answer.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class CORGConfig{
    @Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**")
				 .allowCredentials(true)
                /*
                 允许任何域名使用
                 */
                .allowedOrigins("*")
                /*
                允许 "POST", "GET", "PUT", "OPTIONS", "DELETE"方法
                 */
                .allowedMethods("POST", "GET", "PUT", "OPTIONS", "DELETE")
                /*
                允许任何头
                 */
                .allowedHeaders("*"); 		
			}
		};
    }
}