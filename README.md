Server Hỏi đáp bác sĩ:

Buid server: 
  - mvn clean install

Run server:
  - cd target ; java -jar minishop-0.0.1-SNAPSHOT.jar

Links document:
  - http://localhost:8080/swagger-ui.html


Take the compiled code and package it in its distributable format:
  - mvn package